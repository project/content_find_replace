jQuery('document').ready(function(){
	checkCompletionReplace();
});

var per = 0;

//Reccusrive function to Continously check DATABASE table update complete
function checkCompletionReplace(){
	setTimeout('completionProgressBar()', 1000 );
	var path = window.location.pathname + "/ajax";
	jQuery.ajax({
		  url: path,
		  success: function(busy){
			setTimeout('checkCompletionReplaceComplete()', 1000 );
		  }
	});
}

//Progress bar to update the text
function completionProgressBar(){
	per += 12;
	$('#updateprogress .percentage').text(per + '%');
	$('#updateprogress .filled').attr('style', 'width:' + per + '%');
	if(per > 100) setTimeout('completionProgressBar()', 3000 );
}

//Progress bar to update the text
function checkCompletionReplaceComplete(){
	per = 100;
	$('#updateprogress .percentage').text('100%');
	$('#updateprogress .bar > div').attr('class', 'filled');
	$('#updateprogress .filled').attr('style', 'width:100%');
	$('#updateprogress .message').text('String replace complete.');
	window.location.replace(window.location.href.replace('/replace','/report'));
}