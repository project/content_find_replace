<?php
// $Id: string_database_replace.admin.inc, 2012/03/2 20:36:43 sudev Exp $

/**
 * @file
 * Admin page callbacks for the string database replace module.
 */

/**
 * Menu callback for the String Overrides module to display its administration
 */
function string_database_replace_admin(){
  $admin_html = drupal_get_form('string_database_replace_admin_form');
  $admin_html .= (arg(3)) ? '' : string_database_replace_admin_table();
  $admin_html .= (arg(4) == 'replace') ? string_database_replace_progress() : '';
  return (arg(4) != 'report') ? $admin_html : string_database_replace_report(arg(3));
}

/**
 * Function to add the html table for selecting 
 * older string replace settings tp preform the operation again. 
 */
function string_database_replace_admin_table(){
  $header = array(t('Title'), t('Find string'), t('Replace string'), t('Replace'));
  $completedata = variable_get('string_database_replace', array());
  foreach($completedata as $k => $v){
    $rows[$k]['title'] = $k;
    $rows[$k]['original'] = $v['original'];
    $rows[$k]['replacement'] = $v['replacement'];
    $rows[$k]['body'] = l('Replace text', 'admin/settings/sdr/' . $k);
  }
  return theme_table($header, $rows);
}

/**
 * Form for the administration settings and entering replacement strings.
 */
function string_database_replace_admin_form(){
  $form['database_op'] = array(
      '#type' => 'hidden',
      '#value' => 0,
  );
  if(!arg(3)){
    $form['string_database_replace'] = array(
      '#type' => 'fieldset',
      '#title' => t('Find and replace'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['string_database_replace']['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#description' => t('A unique identifier for the replacement. Please include only alphanumeric characters and underscores.')
    );
    $form['string_database_replace']['original'] = array(
      '#type' => 'textfield',
      '#title' => t('Find'),
      '#description' => t('The string which exists in the database and should be replaced.')
    );
    $form['string_database_replace']['replacement'] = array(
      '#type' => 'textfield',
      '#title' => t('Replace'),
      '#description' => t('The string to replace the original string by.')
    );
    foreach(node_get_types() as $k => $v) $options[$k] = $v->name;
    $form['string_database_replace']['nodetypes'] = array(
      '#type' => 'select',
      '#title' => t('Select content type'),
      '#description' => t('Please select the content type for which the string is to be replaced.'),
      '#options' => $options
    );
    $form['database_op']['#value'] = 1;
    $savetext = 'Save changes';
  }
  else{
    $savetext = 'Replace';
    $completedata = variable_get('string_database_replace', array());
    $query = db_query("SELECT label, field_name FROM content_node_field_instance where type_name='%s' and widget_module='text'", $completedata[arg(3)]['nodetype']);
    $names = array('title' => 'Title', 'body' => 'Body', 'teaser' => 'Teaser');
    while($row = db_fetch_object($query)){
      $names[$row->field_name] = $row->label;
    }
    $def_names = array();;
    if($completedata[arg(3)]['fields'])
    foreach($completedata[arg(3)]['fields'] as $k => $v){
      if($v) $def_names[] = $k;
    }
    $disabled = (arg(4)) ? TRUE : FALSE;
    $form['string_database_replace']['fields'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Select specific fields'),
      '#description' => t('Please select specific fields in ' . $completedata[arg(3)]['nodetype'] . ' to replace the content within with the specified text.'),
      '#options' => $names,
      '#default_value' => $def_names,
      '#disabled' => $disabled
    );
    
    $form['back_button'] = array(
      '#type' => 'button',
      '#value' => 'Back',
      '#attributes' => array(
         'class' => 'button-user',
         'onclick' => "window.location.url = 'https://www.google.co.in/'",        
      )
    );
  }
  $form['#submit'] = array('string_database_replace_admin_form_submit');
  $form['save_submit'] = array(
    '#type' => 'submit',
    '#value' => t($savetext),
  );
  return $form;
}

function string_database_replace_admin_form_validate($form, &$form_state){
  $form_state['values']['title'] = str_replace(" ", "_", $form_state['values']['title']);
 if(!preg_match('/^[A-Za-z0-9_]+$/',$form_state['values']['title']) && $form_state['values']['database_op']) 
   form_set_error('title');
}

/**
 * Submit function for storing the new replacement string in variable.
 */
function string_database_replace_admin_form_submit($form, $form_state){
  $completedata = variable_get('string_database_replace', array());
  if($form_state['values']['database_op']){
    $savedata = array('original' => $form_state['values']['original'], 
                 	  'replacement' => $form_state['values']['replacement'],
                      'nodetype' => $form_state['values']['nodetypes'],
                      'fields' => '');
    $completedata[$form_state['values']['title']] = $savedata;
    variable_set('string_database_replace', $completedata);
    $redirect = $_GET['q'] . '/' . $form_state['values']['title'];
    drupal_set_message('The selected content has been replaced');
    drupal_redirect_form($form, $redirect);
  }else{
    $data = array();
    foreach($form_state['values']['fields'] as $k => $v)
      if($v) $data = $form_state['values']['fields'];
    $completedata[arg(3)]['fields'] = $data;
    variable_set('string_database_replace', $completedata);
    drupal_redirect_form($form, arg(0) . '/' . arg(1) . '/' . arg(2) . '/' . arg(3) . '/' . 'replace');
  }
}

/**
 * Helper function to add the html for the progress bar for node save op.
 * @return rendered HTML
 */
function string_database_replace_progress(){
  drupal_add_js(drupal_get_path('module', 'string_database_replace') . '/js/string_database_replace_ping.js');
  $completedata = variable_get('string_database_replace', array());
  if(arg(5) == 'ajax') string_database_replace_update_database($completedata[arg(3)]);
  return '
  <h3>Please do not close or redirect from this window</h3>
  <div id="progress" class="batch-processed">
  	<div id="updateprogress" class="progress">
      <div class="bar">
      	<div class="filled" style="width: 0%;"></div>
      </div>
  	  <div class="message">String replace in progress</div>
  	  <div class="percentage">0%</div>
  	</div>
  </div>';
}


/**
 * Helper function to update every node of the desired content type.
 * @param $info
 * @return (boolean)TRUE
 */
function string_database_replace_update_database($info){
  $nids = array();
  $query = db_query("SELECT nid FROM node where type='%s'", $info['nodetype']);
  while($row = db_fetch_object($query)){
    $node = node_load($row->nid);
    foreach($info['fields'] as $k => $v){
      $cck = (substr($v, 0, 6) == 'field_' && mb_substr($v, 0, 6) == 'field_') ? TRUE : FALSE;
      string_database_replace_update_database_cck($node->$v, $info, $cck);
    } 
    $nids[] = $row->nid;
    node_save($node);
  }
  $logsdata = variable_get('string_database_replace_logs', array());
  $logsdata[arg(3)] = implode(',', $nids);
  variable_set('string_database_replace_logs', $logsdata);
  /*foreach($info['fields'] as $k => $v){
    if($v && $k != 'body'){
      $update = "UPDATE {content_type_" . $info['nodetype'] ."} SET " . $v . " = replace(" . $v . ", '" . $info['original']. "', '" . $info['replacement']. "');";
    }elseif($k == 'body'){
      $update = "UPDATE content_type_" . $info['nodetype'] ." SET " . $v . " = replace(" . $v . ", '" . $info['original']. "', '" . $info['replacement']. "');";
    }elseif($k == 'title'){
      $update = "UPDATE content_type_" . $info['nodetype'] ." SET " . $v . " = replace(" . $v . ", '" . $info['original']. "', '" . $info['replacement']. "');";
    }
    if($update) drupal_set_message($update);
  }*/
  return TRUE;
}
/**
 * Helper function to update the cck values with replace string.
 * @param $node_cck
 * @param $info
 * @param $cck
 */
function string_database_replace_update_database_cck(&$node_cck, $info, $cck = TRUE){
  if (!$cck) {
    $node_cck = str_replace($info['original'], $info['replacement'], $node_cck); 
  }else{
    foreach($node_cck as $k => $v){
      $node_cck[$k] = str_replace($info['original'], $info['replacement'], $v);
    }
  }
}

function string_database_replace_report($name){
  $header = array(t('No.'), t('Title'), t('Edit'));
  $logsdata = variable_get('string_database_replace_logs', array());
  $nids = explode(",", $logsdata[arg(3)]);
  foreach($nids as $k => $v){
    $title = db_result(db_query("SELECT title from node where nid=%d", $v));
    $rows[$k]['count'] = $k + 1;
    $rows[$k]['title'] = l($title, 'node/'.$v);
    $rows[$k]['edit'] = l('edit', 'node/'.$v.'/edit');
  }
  $html = theme_table($header, $rows);
  $html .= l('<< Return to find and replace', 'admin/settings/sdr');
  return $html;
}
